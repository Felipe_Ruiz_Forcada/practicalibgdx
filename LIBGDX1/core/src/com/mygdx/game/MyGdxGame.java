package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

public class MyGdxGame extends ApplicationAdapter implements InputProcessor {
	SpriteBatch batch;
	Texture img;
	Rectangle Rect;
	Rectangle	sprite;
	float timeControl;
	float vel;
	float velSpeed;
	float minSpeed;
	boolean turn;
	Animation Anim;
	TextureRegion txture;
	float moveSpeed;
	float pich;
	long idSound;
	float Time;
	float endTime;
	float maxSpeed;
	float speed;
	boolean inactive;
	TextureAtlas AtlasTexture;
	Sound MotorSound;

	boolean run;

	@Override
	public void create () {
	/*	public void create ()
		{ batch = new SpriteBatch();
			texAtlas = new TextureAtlas(Gdx.files.internal("atlas/spritesheet.atlas"));
			TextureAtlas.AtlasRegion region = texAtlas.findRegion("a1");
			sprite = new Sprite(region); sprite.setPosition(120, 100);
			sprite.scale(4f);
			Timer.schedule(new Task()
			{ @Override public void run()
			{ currentFrame++; if (currentFrame > 7) currentFrame = 1; currentAtlasKey = "a" + String.format("%01d", currentFrame);
				sprite.setRegion(texAtlas.findRegion(currentAtlasKey)); } } , 1, 1 / 2.0f);
		}*/
		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");
		Gdx.app.log("Control","Enable");
		AtlasTexture = new TextureAtlas(Gdx.files.internal("Atlas.atlas"));
		Anim = new Animation(1/16f,AtlasTexture.getRegions());
		Rect = new Rectangle();
		velSpeed = 0f;
		Gdx.input.setInputProcessor(this);
		turn=false;
		Rect.x=200;
		Rect.y=200;
		txture = new TextureRegion();
		MotorSound=Gdx.audio.newSound(Gdx.files.internal("345925__1histori__car-engine.wav"));
		idSound=MotorSound.play();
		MotorSound.setLooping(idSound,true);
		pich=0;
		speed =0;
		maxSpeed=200;
		minSpeed=0;
		vel=250f;
		inactive=true;


	}
	public void resize(int width, int height)
	{
		Gdx.app.log("Control","NewSize");
	}


	@Override
	public void render ()
	{
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		batch.draw(img, 0, 0);
		batch.end();
		Rect.x+=velSpeed*endTime;
		/*Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		sprite.draw(batch);*/
		if (run)
		{
			if (speed<maxSpeed) speed+=vel*endTime;
			if(speed>maxSpeed) speed=maxSpeed;
		}
		else
		{
			if (!inactive)
			{
				speed -=vel*endTime;
				if (speed <= minSpeed) {
                    idSound = MotorSound.play();
					//MotorSound.setLooping(idSound, true);
					speed = minSpeed;
					inactive = true;
				}

			}
		}

		batch.begin();
		txture.setRegion((TextureRegion)Anim.getKeyFrame(Time,true));
		txture.flip(turn,false);
		Time += Gdx.graphics.getDeltaTime();
		endTime = Gdx.graphics.getDeltaTime();
		batch.draw(txture,Rect.x,Rect.y);
		batch.end();
	}
	public void pause() {
		Gdx.app.log("Control","Stop");
	}

	@Override
	public void resume() {
		Gdx.app.log("Control","Retry");
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
	}
	public boolean keyDown(int keycode) {

		if(keycode == Input.Keys.LEFT)
		{
			turn = true;
			velSpeed = -1000f;
		}
		if(keycode == Input.Keys.RIGHT)
		{
			turn = false;
			velSpeed = 1000f;
		}
		if(keycode == Input.Keys.SPACE){
			run	 = true;
			pich = 0.5f + speed / maxSpeed * 0.5f;
			MotorSound.setPitch(idSound, pich);
			//idSound = MotorSound.play();
		//	MotorSound.setLooping(idSound,true);
			inactive = false;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		velSpeed = 0f;
		if(keycode == Input.Keys.SPACE) run	 = false;

		return false;

	}

	public boolean keyTyped(char character)
	{
		return false;
	}


	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if(button ==0){
			Rect.x = screenX - txture.getRegionWidth()/2;
			Rect.y = (Gdx.graphics.getHeight() - screenY) - txture.getRegionHeight()/2;
		}
		return false;
	}


	public boolean touchUp(int screenX, int screenY, int pointer, int button)
	{
		return false;
	}


	public boolean touchDragged(int screenX, int screenY, int pointer)
	{
		return false;
	}


	public boolean mouseMoved(int screenX, int screenY)
	{
		return false;
	}


	public boolean scrolled(int amount)
	{
		return false;
	}
}
